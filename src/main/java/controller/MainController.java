package controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import service.BidService;
import service.BuyerService;
import service.ProductService;
import service.SellerService;

import java.sql.SQLException;


public class MainController {

    private final ProductService productService = new ProductService();

    @FXML
    private TextField cityTextField;

    @FXML
    private TextField streetTextField;

    @FXML
    private TextField numberTextField;

    @FXML
    private TextField sellerIdTextField;


    @FXML
    private void saveProduct() throws SQLException {
        productService.saveProduct(cityTextField.getText(), streetTextField.getText(),
                Integer.parseInt(numberTextField.getText()), Integer.parseInt(sellerIdTextField.getText()));
    }


    private final SellerService sellerService = new SellerService();

    @FXML
    private TextField sellerNameTextField;

    @FXML
    private void saveSeller() throws SQLException {
        sellerService.saveSeller(sellerNameTextField.getText());
    }

    private final BuyerService buyerService = new BuyerService();

    @FXML
    private TextField buyeFirstrNameTextField;

    @FXML
    private TextField buyerLastNameTextField;

    @FXML
    private void saveBuyer() throws SQLException {
        buyerService.saveBuyer(buyeFirstrNameTextField.getText(), buyerLastNameTextField.getText());
    }

    private BidService bidService = new BidService();

    @FXML
    private TextField nominalTextField;

    @FXML
    private TextField buyerIdTextField;

    @FXML
    private TextField productIdTextField;

    @FXML
    private void saveBid() throws SQLException {
        bidService.saveBid(Integer.parseInt(nominalTextField.getText()),
                Integer.parseInt(buyerIdTextField.getText()),
                Integer.parseInt(productIdTextField.getText()));
    }
}
